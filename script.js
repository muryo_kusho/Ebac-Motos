$(document).ready(function() {
    $('#telefone').mask('(00) 00000-0000');

    $('#contact-form').validate({
        rules: {
            nome: 'required',
            email: {
                required: true,
                email: true
            },
            mensagem: 'required'
        },
        messages: {
            nome: 'Por favor, insira seu nome',
            email: {
                required: 'Por favor, insira seu e-mail',
                email: 'Por favor, insira um e-mail válido'
            },
            mensagem: 'Por favor, insira sua mensagem'
        },
        submitHandler: function(form) {
            alert('Formulário enviado com sucesso!');
        }
    });
});